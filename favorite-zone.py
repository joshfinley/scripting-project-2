# imports
import sys
import re

# import our favorites file to use dictionary methods
from favorites import faves as faves_dict

# open the file as text
names = open("favorites.py", "r")

# gather info about the program and usage
program_name = sys.argv[0]
arguments = sys.argv[1:]
count = len(arguments)

# check if usage is correct
if len (sys.argv) != 1 :
    print("Usage: python favorite-zone.py")
    sys.exit (1)

print("################################")
print("--WELCOME TO THE fAvOrItE zOnE--")
print("################################")

# This program utilizes three main functions
# This function accepts use input and then searches
# for a name
def get_faves_by_name():
    while True:
        print("\nPlease enter the first name of the person to search for")
        user_input = str(input("> ").capitalize())
        user_key = str(faves_dict.get(user_input))

        # make sure the input is correct
        if not user_input or len(user_input) == 1:
            print("Pleae provide a name")
        elif user_key:
            # Get required data
            fave_color = re.search(r'\'(.+?)\'', user_key).group(1)
            fave_number = re.search(r"\d+", user_key).group(0)

            # format and print regexs
            print("{0}'s favorite color is {1} and his/her favorite number is {2}".format(
                user_input, fave_color, fave_number
            ))

            # Call next function an alg
            get_info_by_char()
            
            break
        
        # If input seemed valid but
        # no results were found, say so
        elif not faves_dict.get(user_input):
            print("No results found")

# this function asks for a char
# and then returns all matching names and favorites
def get_info_by_char():
    while True:
        print("Please enter a character to see matching names and faves")

        # get character input
        user_input = str(input("> ")).capitalize()
        names = open("favorites.py", "r")

        # begin conditional
        if not user_input:
            # if user did not give input, call first func
            get_faves_by_name()
        # if input isnt valid let the user know
        elif len(user_input) > 1 or re.search(r'\d', user_input):
            print("Please only input one letter")
        else:
            # loop through lines in file and regex them for data
            for line in names:
                if re.search(r"\'(" + user_input + ".+?)\'", line):
                    name = re.search(r"(\w+)", line).group(0)
                    fave_color = re.search(r"(?<=\[\')\w+", line).group(0)
                    fave_number = re.search(r"\d+", line).group(0)

                    # format matches
                    print("{0}'s favorite color is {1} and his/her favorite number is {2}".format(
                    name, fave_color, fave_number
                    ))
            
            print("\ntype 'get stats' to get stats")
            cont_input = input("otherwise, hit enter: ")

            if cont_input == 'get stats':
                get_stats()
            else:
                get_faves_by_name()

def get_stats():
    # Print list of favorites colors and how often
    # each was picked
    colors = []
    nums = []

    # loop through file,
    # grab colors and numbers,
    # through those into lists
    for line in names:
        if re.search(r"(?<=\[\')\w+", line):
            color = re.search(r"(?<=\[\')\w+", line).group(0)
            num = re.search(r"\d+", line).group(0)

            colors.append(color)
            nums.append(num)

    # print results
    i = 0
    print("COLOR: \t COUNT: \t NUM: \t COUNT:")
    while i < len(colors) and i < len(nums):
        print("{0} \t {1:2d} \t\t {2} \t {3}".format(colors[i], colors.count(colors[i]), nums[i], nums.count(nums[i])))
        i += 1
    
get_faves_by_name()
